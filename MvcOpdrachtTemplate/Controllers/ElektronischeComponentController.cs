﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcOpdrachtTemplate.BLL;
using MvcOpdrachtTemplate.Models;


namespace MvcOpdrachtTemplate.Controllers
{
    public class ElektronischeComponentController : Controller
    {
        private ElektronischComponentBL _component = new ElektronischComponentBL();
        private CategorieBL _categorie = new CategorieBL();

        // GET: ElektronischeComponent
        public ActionResult List(string sortBy, string searchString)
        {
            ViewBag.Sort = string.IsNullOrEmpty(sortBy) ? "Name desc" : "";
            
            var namen = _component.getAllComponenten().AsQueryable();
           
            if (!String.IsNullOrEmpty(searchString))
            {
                namen = namen.Where(s => s.Naam.Contains(searchString));

            }
            switch (sortBy)
            {
                case "Name desc":
                    namen =namen.OrderByDescending(n => n.Naam);
                    break;
                default:
                    namen =namen.OrderBy(n => n.Naam);
                    break;
            }
            return View(namen.ToList());
        }

        // GET: ElektronischeComponent/Details/5
        public ActionResult Details(int id)
        {
            
            return View(_component.GetComponent(id));
        }

        // GET: ElektronischeComponent/Create
        [Authorize]
        public ActionResult Create()
        {
            ViewBag.CategorieId = new SelectList(_categorie.GetAllCategorien(), "Id", "Naam");
            return View();
        }

        // POST: ElektronischeComponent/Create
        [HttpPost]
        public ActionResult Create(ElektronischComponent component)
        {
            try
            {
                
                _component.CreateComponent(component);

                return RedirectToAction("List");
            }
            catch
            {
                ViewBag.CategorieId = new SelectList(_categorie.GetAllCategorien(), "Id", "Naam");
                return View(component);
            }
        }

        // GET: ElektronischeComponent/Edit/5
        [Authorize]
        public ActionResult Edit(int id)
        {
            ViewBag.CategorieId = new SelectList(_categorie.GetAllCategorien(), "Id", "Naam");
            return View(_component.GetComponent(id));
        }

        // POST: ElektronischeComponent/Edit/5
        [HttpPost]
        public ActionResult Edit(ElektronischComponent component)
        {
            try
            {
                
                _component.UpdateComponent(component);

                return RedirectToAction("List");
            }
            catch
            {
                ViewBag.CategorieId = new SelectList(_categorie.GetAllCategorien(), "Id", "Naam");
                return View(component);
            }
        }

        // GET: ElektronischeComponent/Delete/5
        [Authorize]
        public ActionResult Delete(int id)
        {
            return View(_component.GetComponent(id));
        }

        // POST: ElektronischeComponent/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
             
                _component.DeleteComponent(id);


                return RedirectToAction("List");
            }
            catch
            {
                return View(_component.GetComponent(id));
            }
        }
    }
}
