﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcOpdrachtTemplate;
using MvcOpdrachtTemplate.BLL;
using MvcOpdrachtTemplate.Models;

namespace MvcOpdrachtTemplate.Controllers
{
    public class CategorieController : Controller
    {
        private CategorieBL _categorie = new CategorieBL();
        // GET: Categorie
        public ActionResult List()
        {
            return View(_categorie.GetAllCategorien());
        }

        // GET: Categorie/Details/5
        public ActionResult Details(int id)
        {
            return View(_categorie.GetCategorie(id));
        }

        // GET: Categorie/Create
        [Authorize]
        public ActionResult Create()
        {
            return View();
        }

        // POST: Categorie/Create
        [HttpPost]
        public ActionResult Create(Categorie categorie)
        {
            try
            {
                _categorie.CreateCategorie(categorie);

                return RedirectToAction("List");
            }
            catch
            {
                return View(categorie);
            }
        }

        // GET: Categorie/Edit/
        [Authorize]
        public ActionResult Edit(int id)
        {
            return View(_categorie.GetCategorie(id));
        }

        // POST: Categorie/Edit/5
        [HttpPost]
        public ActionResult Edit(Categorie categorie)
        {
            try
            {
                _categorie.UpdateCategorie(categorie);

                return RedirectToAction("List");
            }
            catch
            {
                return View(categorie);
            }
        }

        // GET: Categorie/Delete/5
        [Authorize]
        public ActionResult Delete(int id)
        {
            return View(_categorie.GetCategorie(id));
        }

        // POST: Categorie/Delete/5
        [HttpPost]
        [ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            try
            {
                _categorie.DeleteCategorie(id);

                return RedirectToAction("List");
            }
            catch
            {
                return View(_categorie.GetCategorie(id));
            }
        }
    }
}
