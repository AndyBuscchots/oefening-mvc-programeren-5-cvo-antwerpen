﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.EnterpriseServices;
using System.Linq;
using System.Web;
using  MvcOpdrachtTemplate.Models;

namespace MvcOpdrachtTemplate.BLL
{
    public class CategorieBL
    {
        private ComponentContext db = new ComponentContext();
        public Categorie GetCategorie(int id)
        {
            return db.Categorie.Find(id);

        }

        public List<Categorie> GetAllCategorien()
        {
            return db.Categorie.ToList();
        }

        public Categorie CreateCategorie(Categorie categorie)
        {
            db.Categorie.Add(categorie);
            db.SaveChanges();
            return categorie;
        }

        public Categorie UpdateCategorie(Categorie categorie)
        {
            db.Entry(categorie).State = EntityState.Modified;
            db.SaveChanges();
            return categorie;
        }

        public void DeleteCategorie(int id)
        {
            Categorie categorie = GetCategorie(id);
            db.Categorie.Remove(categorie);
            db.SaveChanges();
        }
    }
}