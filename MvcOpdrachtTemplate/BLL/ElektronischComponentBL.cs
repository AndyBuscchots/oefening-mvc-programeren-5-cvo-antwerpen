﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MvcOpdrachtTemplate.Models;
using System.Data.Entity;

namespace MvcOpdrachtTemplate.BLL
{
    public class ElektronischComponentBL
    {
       private ComponentContext db = new ComponentContext();
        public ElektronischComponent GetComponent(int id)
        {
            return db.Component.Find(id);
        }

        public List<ElektronischComponent> getAllComponenten()
        {
            return db.Component.Include(c=> c.Categorie).ToList();
        }

        public ElektronischComponent CreateComponent(ElektronischComponent component)
        {

            db.Component.Add(component);
            db.SaveChanges();
            return component;
        }
        public ElektronischComponent UpdateComponent(ElektronischComponent component)
        {
     
            db.Entry(component).State = EntityState.Modified;
            db.SaveChanges();
            return component;

        }

        public void DeleteComponent(int id)
        {
            ElektronischComponent component = GetComponent(id);
            db.Component.Remove(component);
            db.SaveChanges();
        }
        
    }
}