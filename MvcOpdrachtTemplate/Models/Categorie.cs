﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace MvcOpdrachtTemplate.Models
{
    public class Categorie
    {
        [Key]
        public int Id { get; set; }
        public string Naam { get; set; }

        public IList<ElektronischComponent> Componenten { get; set; } 
    }
}