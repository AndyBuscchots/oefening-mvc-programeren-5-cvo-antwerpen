﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MvcOpdrachtTemplate.Models
{
    public class ElektronischComponent
    {

        [Key]
        public int Id { get; set; }
        [Required]
        public string Naam { get; set; }
        public string Link { get; set; }
        public int Aantal { get; set; }
        public double AankoopPrijs { get; set; }
       
        public int CategorieId { get; set; }
        [ForeignKey("CategorieId")]
        public Categorie Categorie { get; set; }
    }
}