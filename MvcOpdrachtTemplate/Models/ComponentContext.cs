﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace MvcOpdrachtTemplate.Models
{
    public class ComponentContext : DbContext
    {
        public ComponentContext() :base()
        {
           
        }

        public DbSet<ElektronischComponent> Component { get; set; }
        public DbSet<Categorie> Categorie { get; set; }
    }
}